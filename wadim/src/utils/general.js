const isInObjValues = (item, obj) => {
  return Object.values(obj).includes(item);
};

module.exports = {
  isInObjValues,
};
