const WITHDRAW_GIFTCARD_TYPES = {
  Fortnite: "Fortnite",
  Visa: "Visa",
  Amazon: "Amazon",
  Steam: "Steam",
  Roblox: "Roblox",
};

const COUNTRY_CODES = {
  US: "US",
  UK: "UK",
  CA: "CA",
  DE: "DE",
  FR: "FR",
  AU: "AU",
  WW: "WW",
};

module.exports = {
  WITHDRAW_GIFTCARD_TYPES,
  COUNTRY_CODES,
};
