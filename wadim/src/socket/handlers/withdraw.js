const db = require("../../db");
const config = require("../../constants/config");
const {
  WITHDRAW_GIFTCARD_TYPES,
  COUNTRY_CODES,
} = require("../../constants/types");
const { isInObjValues } = require("../../utils");
const { validateFields } = require("../helpers/validation");
const { getFeedbackFunc } = require("../helpers/feedback");

let minEarnText = `You must earn at least ${
  config.withdraw.minEarnedToWithdraw
} coins ($${(config.withdraw.minEarnedToWithdraw / 1000).toFixed(
  2
)}) through the offer walls before withdrawing.<br>This is to prevent abuse of the site bonuses. Please contact staff with any questions.`;

module.exports = {
  getGiftcard: function (socket, socketuser) {
    return function (data) {
      if (
        !validateFields(socket, "withdrawFeedback", [
          {
            item: data,
            validateFunc: (x) => x,
            error: "An unknown error occurred",
          },
          {
            item: data.type,
            validateFunc: (x) => type,
            error: "An error occurred. Please try refreshing.",
          },
          {
            item: data.type,
            validateFunc: (x) => isInObjValues(x, WITHDRAW_GIFTCARD_TYPES),
            error: "An error occurred. Please try refreshing.",
          },
          {
            item: data.countryCode || "WW",
            validateFunc: (x) => isInObjValues(x, COUNTRY_CODES),
            error: "An error occurred. Please try refreshing.",
          },
          {
            item: socketuser,
            validateFunc: (x) => socketuser,
            error: "Please login to withdraw!",
          },
          {
            item: parseInt(data.coinAmount),
            validateFunc: (x) => !(isNaN(coinAmount) || !coinAmount),
            error: "Please select an amount!",
          },
        ])
      )
        return;

      const type = data.type,
        coinAmount = parseInt(data.coinAmount),
        countryCode = data.countryCode || "WW";

      const feedback = getFeedbackFunc(socket, "withdrawFeedback", type);

      db.getAccountStanding(socketuser.gainid, (err, standing) => {
        if (err) {
          console.error(err);

          return feedback(`An error occurred, please try again`);
        }
        if (standing.banned || standing.frozen) {
          return feedback(
            `You are currently banned from withdrawing, please contact staff if you believe this is a mistake.`
          );
        }
        db.isDefaultUserEmailVerified(
          socketuser.gainid,
          (err, isEmailVerified) => {
            if (err) {
              console.error(err);

              return feedback(`An error occurred, please try again`);
            }
            if (!isEmailVerified) {
              return feedback(
                `You must verify your E-mail address before requesting a withdrawal!`
              );
            }
            db.getBalanceByGainId(socketuser.gainid, (err, balance) => {
              if (err) {
                console.error(err);
                return feedback(`An error occurred, please try again`);
              }

              if (balance < coinAmount) {
                return feedback(`You don't have enough balance!`);
              }

              db.earnedEnoughToWithdraw(
                socketuser.gainid,
                (err, earnedEnoughBool) => {
                  if (err) {
                    console.error(err);
                    let feedbackText = `An error occurred, please try again.`;
                    return feedback(feedbackText);
                  }

                  if (!earnedEnoughBool) {
                    return feedback(minEarnText);
                  }

                  db.isUserVerified(
                    socketuser.gainid,
                    (err, isUserVerified) => {
                      if (err) {
                        console.error(err);

                        return feedback(`An error occurred, please try again`);
                      }
                      if (isUserVerified) {
                        const result = await GiftcardService.getGiftcard(
                          type,
                          coinPrice,
                          countryCode
                        );
                        if (result) {
                          // TODO
                          db.updateBalanceByGainId(
                            socketuser.gainid,
                            coinPrice * -1,
                            (err) => {
                              if (err) {
                                console.error(err);
                                return feedback(
                                  `An error occurred, please try again`
                                );
                              }
                              db.insertPendingSiteGiftCardWithdraw(
                                socketuser.gainid,
                                coinPrice,
                                type,
                                countryCode,
                                utils.getIsoString(),
                                null,
                                (err, result) => {
                                  if (err) {
                                    console.error(err);
                                    return feedback(
                                      `An error occurred, please try again`
                                    );
                                  }

                                  socket.emit("withdrawalAvailable", {
                                    coins: coinPrice,
                                  });

                                  Notifications.storeNotification(
                                    socketuser.gainid,
                                    "Info",
                                    "pendingwithdrawal",
                                    `Your ${type} Gift Card withdrawal worth ${coinPrice} coins is available.`
                                  );

                                  feedback(
                                    `Success! <br> Check your Profile page to view your redemption code <br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`
                                  );

                                  emitBalance(socketuser.gainid);
                                }
                              );
                            }
                          );
                        }
                        throw new GiftCardUnavailableError(
                          "the gift card is unavailable"
                        );
                      } else {
                        return GiftcardService.isGiftCardInStock(
                          type,
                          coinAmount,
                          countryCode
                        )
                          .then((result) => {
                            // if it's on stock continue as usual
                            if (result) return notVerified();
                            // otherwise, tell the user to pick another card.
                            return feedback(
                              `This card is currently out of stock. Please choose another.`
                            );
                          })
                          .catch((err) => {
                            // if there's an error, it can't be determined if the card is in stock or not
                            console.error(err);
                            return notVerified();
                          });
                      }
                    }
                  );
                }
              );

              function notVerified(outOfStock) {
                db.updateBalanceByGainId(
                  socketuser.gainid,
                  coinAmount * -1,
                  (err) => {
                    if (err) {
                      console.error(err);
                      return feedback(`An error occurred, please try again`);
                    }
                    db.insertPendingSiteGiftCardWithdraw(
                      socketuser.gainid,
                      coinAmount,
                      type,
                      countryCode,
                      utils.getIsoString(),
                      null,
                      (err, result) => {
                        if (err) {
                          console.error(err);
                          return feedback(
                            `An error occurred, please try again`
                          );
                        }

                        socket.emit("withdrawalPending", {
                          coins: coinAmount,
                        });

                        Notifications.storeNotification(
                          socketuser.gainid,
                          "Info",
                          "pendingwithdrawal",
                          `Your ${type} Gift Card withdrawal worth ${coinAmount} coins is pending.`
                        );

                        if (outOfStock) {
                          feedback(
                            `Success!<br>This card is currently out of stock. A staff member will approve your withdrawal when it is restocked.`
                          );
                        } else {
                          feedback(
                            `Success!<br>Since you are not verified, a staff member has been notified and will review your withdrawal shortly! Check your Profile page to view your redemption code after the withdrawal has been approved.<br><br>Have an opinion on our site? Share it by <a href="https://trustpilot.com/evaluate/freecash.com" target="_blank">writing a Trustpilot review</a>!`
                          );
                        }

                        emitBalance(socketuser.gainid);
                      }
                    );
                  }
                );
              }
            });
          }
        );
      });
    };
  },
};

class GiftCardUnavailableError extends Error {}
