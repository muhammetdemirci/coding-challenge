function getFeedbackFunc(socket, eventName, feedbackType = "") {
  return function (feedback = "An error occurred. Please try refreshing.") {
    socket.emit(eventName, feedback, feedbackType);
  };
}

module.exports = {
  getFeedbackFunc,
};
