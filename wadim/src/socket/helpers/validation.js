const { getFeedbackFunc } = require("./feedback");

const validateFields = (socket, eventName, array) => {
  const feedback = getFeedbackFunc(socket, eventName);

  for (let index = 0; index < array.length; index++) {
    const element = array[index];

    if (element.item && element.validateFunc) {
      if (!element.validateFunc(element.item)) {
        feedback(element.error);
        return false;
      }
    }
  }
  return true;
};

module.exports = {
  validateFields,
};
