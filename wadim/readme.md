# https://rowan-beginner-231.notion.site/Freecash-Coding-Challenge-aabbf0036f4840d2b06b0bcee630e554

Thanks so much for your interest in working at [Freecash](https://freecash.com) as a Full-Stack Engineer! Below is a challenge we'd like you to do.

**You should spend no more than 2 hours on this project.**

The first part will be a refactoring challenge, the second part is a small feature extension.

**Challenge Code:**

[freecash-challenge.zip](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/54ea5578-49a0-45e5-a7d1-7a9b3b5ab02d/freecash-challenge.zip)

### 1. Refactor Code

On the site users can earn coins, which they later can use to withdraw for gift cards, PayPal balance, cryptocurrencies:

![Untitled](https://s3-us-west-2.amazonaws.com/secure.notion-static.com/7fc9e950-9463-4525-9d13-adcd9fc2ead7/Untitled.png)

When a user withdraws the withdrawal is sent to the backend via websocket. The user gets deducted the amount of coins and the withdrawal becomes pending until reviewed by a moderator. A moderator will later either release or reject the withdrawal. For all withdrawal types the process would be very similar.

In the given code you will find an older implementation of a gift card withdrawal. The first task is to refactor the implementation. Feel free to add other npm packages, to create new files, and to include notes or text documents where you explain your reasoning. You don't need to write tests and the code also doesn't need to run. We'll be just checking the general structure.

The database used is MySQL.

### 2. Add a small feature

Users can become verified (once they reach 100$ in earnings an admin can verify a user). Verified users skip moderator approval for their withdrawals, and can withdraw instantly. The verification state is tracked via a `verified` column in the users table. The type of the column is a `tinyint(1)`.

The task is to add the gift card withdrawals for verified users. You don't need to code the logic of buying the gift card (just use the `getGiftcard` placeholder function from below).

Please use the following sample function: `GiftcardService.getGiftcard(type: string, coinPrice: number, countryCode: string): Promise<giftcardCode>`

If the gift card is unavailable the function will throw a `GiftCardUnavailableError` error. You then need to send back an error message to the user, notifying him that the gift card is unavailable.

If the withdrawal goes through you need to insert the withdrawal into the database, deduct the coins from the users balance, create a notification, and send back a message to the user telling him that the withdrawal was successful and also include the gift card code in the message.

For inserting the withdrawal into the database you can use the `db.insertSiteGiftCardWithdrawal` in the database file as an example (feel free to change it).
